package net.jb;

import javax.swing.*;
import java.awt.*;

public class RootFrame extends JFrame {
    private static final RootFrame INSTANCE = new RootFrame();

    private RootFrame() {}

    public static Runnable createNewFrame() {
        return () -> {
            /* CAUSES ERROR
            try {
                UIManager.setLookAndFeel("com.fromdev.flatlaf.FlatLightLaf");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(INSTANCE, "Error setting look and feel. Will use default theme.", "Error", JOptionPane.ERROR_MESSAGE);
            }
            */

            INSTANCE.setTitle("Image Sorter");
            INSTANCE.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            INSTANCE.setResizable(false);

            INSTANCE.add(new PathSelPanel(), BorderLayout.WEST);
            INSTANCE.add(new StartPanel(), BorderLayout.SOUTH);

            INSTANCE.pack();
            INSTANCE.setLocationRelativeTo(null);
            INSTANCE.setVisible(true);
        };
    }

    public static RootFrame getInstance() {
        return INSTANCE;
    }
}
