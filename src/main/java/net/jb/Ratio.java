package net.jb;

public class Ratio {
    public static String getRatio(JBImage image) {
        double ratio = (double) image.getWidth() / image.getHeight();
        if (ratio <= 0.9001) {
            return "10:11";
        } else if (ratio == 1) {
            return "1:1";
        } else if (ratio >= 1.1001) {
            return "9:10";
        } else {
            return null;
        }
    }
}
