package net.jb;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Objects;

public class StartPanel extends JPanel {
    public StartPanel() {
        JButton start = new JButton("Start");
        start.setToolTipText("Starts the sorting process");
        JButton help = new JButton("?");
        help.setToolTipText("Shows help message");
        JProgressBar progressBar = new JProgressBar(0, 100);
        progressBar.setPreferredSize(new Dimension(80, 20));
        JLabel progressLabel = new JLabel("Progress: Idle");

        start.addActionListener(e -> {
            if (Config.inputDirPath == null || Config.outputDirPath == null) {
                JOptionPane.showMessageDialog(RootFrame.getInstance(), "Please set input and output paths", "Error", JOptionPane.ERROR_MESSAGE);
            } else if (Config.inputDirPath.equals(Config.outputDirPath)) {
                JOptionPane.showMessageDialog(RootFrame.getInstance(), "Input and output paths cannot be the same", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                progressLabel.setText("Progress: Searching for Images");
                for (File file : Objects.requireNonNull(Config.inputDirPath.listFiles())) {
                    if (file.isFile()) {
                        String fileName = file.getName();
                        String extension = fileName.substring(fileName.lastIndexOf(".") + 1);
                        if (extension.equals("png") || extension.equals("jpg") || extension.equals("jpeg") || extension.equals("jpe") || extension.equals("jif") || extension.equals("jifi")) {
                            try {
                                Config.files.add(new JBImage(file));
                            } catch (Exception ex) {
                                JOptionPane.showMessageDialog(RootFrame.getInstance(), "Error reading file: " + file.getName(), "Error", JOptionPane.ERROR_MESSAGE);
                            }
                        }
                    }
                }
                progressBar.setMaximum(Config.files.size());

                progressLabel.setText("Progress: Creating Directories");
                File sortedDir = new File(Config.outputDirPath + "/sorted");
                File wideDir = new File(sortedDir + "/wide");
                File tallDir = new File(sortedDir + "/tall");
                File squareDir = new File(sortedDir + "/square");

                if (!sortedDir.exists())
                    if (!sortedDir.mkdir()) {
                        JOptionPane.showMessageDialog(RootFrame.getInstance(), "Error creating directory: " + sortedDir.getName(), "Error", JOptionPane.ERROR_MESSAGE);
                        progressLabel.setText("Progress: Idle");
                        return;
                    }
                if (!wideDir.exists())
                    if (!wideDir.mkdir()) {
                        JOptionPane.showMessageDialog(RootFrame.getInstance(), "Error creating directory: " + wideDir.getName(), "Error", JOptionPane.ERROR_MESSAGE);
                        progressLabel.setText("Progress: Idle");
                        return;
                    }
                if (!tallDir.exists())
                    if (!tallDir.mkdir()) {
                        JOptionPane.showMessageDialog(RootFrame.getInstance(), "Error creating directory: " + tallDir.getName(), "Error", JOptionPane.ERROR_MESSAGE);
                        progressLabel.setText("Progress: Idle");
                        return;
                    }
                if (!squareDir.exists())
                    if (!squareDir.mkdir()) {
                        JOptionPane.showMessageDialog(RootFrame.getInstance(), "Error creating directory: " + squareDir.getName(), "Error", JOptionPane.ERROR_MESSAGE);
                        progressLabel.setText("Progress: Idle");
                        return;
                    }

                for (JBImage image : Config.files) {
                    progressLabel.setText("Progress: Processing " + image.getFile().getName());
                    progressBar.setValue(progressBar.getValue() + 1);
                    if (Objects.equals(Ratio.getRatio(image), "9:10")) {
                        image.move(wideDir);
                    } else if (Objects.equals(Ratio.getRatio(image), "10:11")) {
                        image.move(tallDir);
                    } else if (Objects.equals(Ratio.getRatio(image), "1:1")) {
                        image.move(squareDir);
                    }
                }

                progressLabel.setText("Progress: Complete");
                JOptionPane.showMessageDialog(RootFrame.getInstance(), "Complete!", "Complete", JOptionPane.INFORMATION_MESSAGE);
                progressLabel.setText("Progress: Idle");
                progressBar.setValue(0);
            }
        });

        help.addActionListener(e -> JOptionPane.showMessageDialog(RootFrame.getInstance(), "Supports PNG and JPG (jpg, jpeg, jpe, jif, jifi)", "Help", JOptionPane.INFORMATION_MESSAGE));

        this.add(help);
        this.add(start);
        this.add(progressBar);
        this.add(progressLabel);
    }
}