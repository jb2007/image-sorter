package net.jb;

import javax.swing.*;

public class GuiStart {

    public static void main(String[] args) { createAndShowGUI(); }
    private static void createAndShowGUI() {
        SwingUtilities.invokeLater(RootFrame.createNewFrame());
    }
}