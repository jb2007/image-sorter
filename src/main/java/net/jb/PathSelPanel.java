package net.jb;

import javax.swing.*;
import java.awt.*;

public class PathSelPanel extends JPanel {

    public PathSelPanel() {
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        this.setPreferredSize(new Dimension(500, 70));

        JButton inputPath = new JButton("Input");
        inputPath.setToolTipText("Select the directory containing the images to be sorted.");
        JButton outputPath = new JButton("Output");
        outputPath.setToolTipText("Select the directory to move the images to.");

        JLabel inputLabel = new JLabel("[Path Not Set]");
        JLabel outputLabel = new JLabel("[Path Not Set]");

        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addComponent(inputPath)
                        .addComponent(inputLabel))
                .addGroup(layout.createSequentialGroup()
                        .addComponent(outputPath)
                        .addComponent(outputLabel)));

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(inputLabel)
                        .addComponent(inputPath))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(outputLabel)
                        .addComponent(outputPath)));

        inputPath.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnVal = fileChooser.showOpenDialog(RootFrame.getInstance());

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                Config.inputDirPath = fileChooser.getSelectedFile();
                inputLabel.setText(Config.inputDirPath.toString());
            }
        });

        outputPath.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnVal = fileChooser.showOpenDialog(RootFrame.getInstance());

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                Config.outputDirPath = fileChooser.getSelectedFile();
                outputLabel.setText(Config.outputDirPath.toString());
            }
        });
    }
}