package net.jb;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class JBImage {

    protected File file;
    protected BufferedImage image;
    protected Integer width;
    protected Integer height;

    public JBImage(File file) throws IOException {
        this.file = file;
        image = ImageIO.read(file);
        width = image.getWidth();
        height = image.getHeight();
    }

    public void move(File path) {
        File newFile = new File(path + "/" + file.getName());
        if (!file.renameTo(newFile)) {
            JOptionPane.showMessageDialog(RootFrame.getInstance(), "Error moving file: " + file.getName(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public File getFile() { return file; }

    public BufferedImage getImage() { return image; }

    public Integer getWidth() { return width; }

    public Integer getHeight() { return height; }
}
